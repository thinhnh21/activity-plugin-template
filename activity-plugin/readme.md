The project may also be compiled from the command line by executing the `dev` NPM script:
step 1: npm install
step 2: config your component : remind your ID activity in ---src\\yourcomponent plugin.ts--- file
step 3: npm run dev

# Build done Output #

Then
+ Copy file [...].plugin.js form .\dist\
	to 
		{your project automation}\sitecore\shell\client\Applications\MarketingAutomation\plugins 
		(or create a new folder in here)(* if you creat new folder, you must be config path "./plugins/new folder/....")

+ setup file Feature.Automation.config in folder:  {your project automation}\App_Config\Include\Feature\
	<!-- <configuration xmlns:patch="http://www.sitecore.net/xmlconfig/" xmlns:set="http://www.sitecore.net/xmlconfig/set/" xmlns:role="http://www.sitecore.net/xmlconfig/role/">
		<sitecore role:require="Standalone or ContentManagement">
			<marketingAutomation>
				<pluginDescriptorsRepository>
					<plugins>
						<plugin path="./plugins/sendlinemessage.plugin.js"/> (*)
						<plugin path="./plugins/sendsmsmessage.plugin.js"/>
					</plugins>
				</pluginDescriptorsRepository>
			</marketingAutomation>
		</sitecore>
	</configuration> -->
+ done.

