﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SMSContentComponent } from './editor/sms-content.component';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [SMSContentComponent],
    entryComponents: [SMSContentComponent]
})
export class SendSMSMessageModule { }
