﻿import { SingleItem} from '@sitecore/ma-core';

export class SendSMSMessageActivity extends SingleItem {
	
    getVisual(): string {
        const subTitle = 'Send SMS Message';
        const cssClass = this.isDefined ? '' : 'undefined';
        
        return `
            <div class="viewport-readonly-editor marketing-action ${cssClass}">
                <span class="icon">
                    <img src="/~/icon/OfficeWhite/32x32/mail_forward.png" />
                </span>
                <p class="text with-subtitle" title="Send SMS Message">
                    Send SMS Message
                    <small class="subtitle" title="${subTitle}">${subTitle}</small>
                </p>
            </div>
        `;
    }

    get isDefined(): boolean {
        return true;
    }
}