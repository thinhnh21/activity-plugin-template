﻿import { Component, OnInit, Injector } from "@angular/core";
import { EditorBase } from "@sitecore/ma-core";

@Component({
  selector: "readonly-editor",
  template: `
      <section class="content">
        <div class="form-group">
            <div class="row readonly-editor">
                <label class="col-12 title">Notification</label>
            </div>
            <div class="row">
                <div class="col-12">
                    <input type="text" class="form-control" [(ngModel)]="body" />
                </div>
            </div>
        </div>
      </section>
  `,
  //CSS Styles are ommitted for brevity
  styles: [""]
})
export class SMSContentComponent extends EditorBase implements OnInit {
  constructor(private injector: Injector) {
    super();
  }

  body: Text;

  ngOnInit(): void {
    this.body = this.model.body || "Thanks for registering for the event, it will be outstanding.";
  }

  serialize(): any {
    return {
      Body: this.body
    };
  }
}