﻿import { Plugin } from '@sitecore/ma-core';
import { SendSMSMessageActivity } from './send-sms-message-activity';
import { SendSMSMessageModuleNgFactory } from '../../codegen/send-sms-message/send-sms-message-module.ngfactory';
import { SMSContentComponent } from '../../codegen/send-sms-message/editor/sms-content.component';

// Use the @Plugin decorator to define all the activities the module contains.
@Plugin({
    activityDefinitions: [
        {
            // The ID must match the ID of the activity type description definition item in the CMS.
            id: 'FECD89AE-2204-4917-B54D-215E0B9AA1EB', 
            activity: SendSMSMessageActivity,
            editorComponenet: SMSContentComponent,
            editorModuleFactory: SendSMSMessageModuleNgFactory
        }
    ]
})
export default class SendSMSMessagePlugin {}
