﻿import { SingleItem } from "@sitecore/ma-core";

export class SendLineMessageActivity extends SingleItem {
    getVisual(): string {
        const subTitle = "Send line notification";
        const cssClass = this.isDefined ? "" : "undefined";

        return `
            <div class="viewport-readonly-editor marketing-action ${cssClass}">
                <span class="icon">
                    <img src="/~/icon/OfficeWhite/32x32/mobile_phone.png" />
                </span>
                <p class="text with-subtitle" title="Send line notification">
                    Send line notification
                    <small class="subtitle" title="${subTitle}">${subTitle}</small>
                </p>
            </div>
        `;
    }

    get isDefined(): boolean {
        return true;
    }
}