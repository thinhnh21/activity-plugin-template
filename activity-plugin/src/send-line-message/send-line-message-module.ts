﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LineContentComponent } from './editor/line-Content.component';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [LineContentComponent],
    entryComponents: [LineContentComponent]
})
export class SendLineMessageModule { }
