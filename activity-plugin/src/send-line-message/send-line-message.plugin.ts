﻿import { Plugin } from '@sitecore/ma-core';
import { SendLineMessageActivity } from './send-line-message-activity';
import { SendLineMessageModuleNgFactory } from '../../codegen/send-line-message/send-line-message-module.ngfactory';
import { LineContentComponent } from '../../codegen/send-line-message/editor/line-Content.component';

// Use the @Plugin decorator to define all the activities the module contains.
@Plugin({
    activityDefinitions: [
        {
            // The ID must match the ID of the activity type description definition item in the CMS.
            id: '7D762575-D27C-4C4F-AFE0-568C88778206', 
            activity: SendLineMessageActivity,
            editorComponenet: LineContentComponent,
            editorModuleFactory: SendLineMessageModuleNgFactory
        }
    ]
})
export default class SendLineMessagePlugin {}
